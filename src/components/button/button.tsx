import style from './button.module.scss'
import '../../fonts/stylesheet.scss'

function Button(props: any) {
    return(
        <button className={props.mode==="submit" ? style.submit : style.cancel} onClick={props.handleSubmitButton}>{props.children}</button>
    )
}

export default Button;