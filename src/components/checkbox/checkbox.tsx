import style from './checkbox.module.scss'
import '../../fonts/stylesheet.scss'

function Checkbox(props: any) {
    return(
        <div className={style.checkbox_wrapper}>
            <input type="checkbox" id={props.id} className={style.checkbox} checked={props.checked} onChange={props.handleCheckbox}></input>
            <label className={style.label} htmlFor={props.id}>{props.label}</label>
        </div>
    )
}

export default Checkbox;
