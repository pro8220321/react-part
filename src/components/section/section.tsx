import style from './section.module.scss'

export function AuthSection(props: any) {
    return (
        <section className="auth-section">
            <div className={style.container}>{props.children}</div>
        </section>
    )
}

export default AuthSection;