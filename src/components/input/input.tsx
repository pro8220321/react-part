import style from './input.module.scss'
import '../../fonts/stylesheet.scss'

function Input(props:any) {
    return(
        <>
            <label className={style.label} htmlFor={props.id}>{props.label}</label>
            {props.valueProps !== undefined  &&  props.setValueProps!== undefined ? 
            <input type={props.type} id={props.id} className={style.input } placeholder={props.placeholder}
            value={props.valueProps !== undefined ? props.valueProps : ''}
            onChange={props.setValueProps!== undefined? (e) =>props.setValueProps(e.target.value) : undefined}></input>
             :<input type={props.type} id={props.id} className={style.input} placeholder={props.placeholder}></input>} 
        </>
    )
}

export default Input;
