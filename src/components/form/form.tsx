import '../../styles/index.scss';
import style from './form.module.scss'
import logo from '../../img/Logo.png'

function AuthForm(props: any){
    return (
        <>
         <div className={style.header}>
            <img className={style.logo} src={logo} alt="logo" />
            <h6 className={style.heading}>{props.heading}</h6>
            {props.subtitle && <h6 className={` ${style.subtitle} ${props.subtitleCenter && style.alignCenter}`}>{props.subtitle}</h6>}
         </div>
        <form className="flex center row">
            {props.children}
        </form>
        </>
    )
}

export default AuthForm;