import { useState } from "react";
import Button from "../components/button/button"
import Checkbox from "../components/checkbox/checkbox";
import Input  from "../components/input/input";
import '../styles/index.scss';
import AuthForm from "../components/form/form";
import AuthSection from '../components/section/section'

function Register() {
    const [checked, setCheked] = useState(false);

    function handleCheckbox() {
        setCheked(!checked)
    }

    return (
            <AuthSection>
                <AuthForm heading="Register your account">
                    <Input id="email" label="Email" type="email" placeholder="name@mail.com"/>
                    <Input id="password" label="Password" type={checked ? "text" : "password"} placeholder="password"/>
                    <Input id="confirm-password" label="Confirm Password" type={checked ? "text" : "password"} placeholder="confirm password"/>
                    <Checkbox  checked={checked} handleCheckbox={handleCheckbox} id="show-password" label="Show Password"/> 
                    <Button mode="submit">Register</Button>
                </AuthForm>
            </AuthSection>
    )
}

export default Register;
