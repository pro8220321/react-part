import { useState } from "react";
import Button from "../components/button/button"
import Input  from "../components/input/input";
import '../styles/index.scss';
import { Link } from "react-router-dom";
import Checkbox from "../components/checkbox/checkbox";
import AuthForm from "../components/form/form";
import AuthSection from '../components/section/section'
function ResetPass() {
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [resetSuccess, setResetSuccess] = useState(false);

    const handleSubmitButton = () => {
        if(newPassword === confirmPassword){
            setResetSuccess(true);
        }else {
            setResetSuccess(false);
        }
    } 

    const [checked, setCheked] = useState(false);

    function handleCheckbox() {
        setCheked(!checked)
    }

    return (
            <AuthSection>
                {resetSuccess ? <><AuthForm heading="Password Changed"
                  subtitleCenter="true"
                  subtitle = "You can use your new password to log into your account">
                        <Link to='/sign-in'>
                            <Button mode="submit">Log in</Button>
                        </Link>
                        <Button mode="cancel">Go to Home</Button>
                    </AuthForm></> 
                :<><AuthForm heading="Reset Your Password">
                    <Input valueProps={newPassword} setValueProps={setNewPassword} id="password" label="New Password" type={checked ? "text" : "password"} placeholder="password"/>
                    <Input valueProps={confirmPassword} setValueProps={setConfirmPassword} id="password" label="Confirm Password"type={checked ? "text" : "password"} placeholder="confirm password"/>
                    <Checkbox checked={checked} handleCheckbox={handleCheckbox} id="show-password" label="Show Password"/>
                    <Button handleSubmitButton={handleSubmitButton} mode="submit">Reset</Button>
                    </AuthForm></>}
            </AuthSection>
        )
}

export default ResetPass;
