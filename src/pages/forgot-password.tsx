import Button from "../components/button/button"
import Input  from "../components/input/input";
import '../styles/index.scss';
import { Link } from "react-router-dom";
import AuthForm from "../components/form/form";
import AuthSection from '../components/section/section'
function ForgotPass() {
    return (
            <AuthSection>
                <AuthForm heading="Reset Password"
                 subtitle="Don't worry, happens to the best of us.
                 Enter the email address associated with
                 your account and we'll send you a link to reset.">
                    <Input id="email" label="Email" type="email" placeholder="name@mail.com"/>
                    <Link to='/reset-password'>
                        <Button mode="submit">Reset</Button>
                    </Link>
                    <Link to='/reset-password'>
                        <Button mode="cancel">Cancel</Button>
                    </Link>
                </AuthForm>
            </AuthSection>
    )
}

export default ForgotPass;
