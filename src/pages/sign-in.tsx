import { useState } from "react";
import Button from "../components/button/button"
import Checkbox from "../components/checkbox/checkbox";
import Input  from "../components/input/input";
import AuthSection from '../components/section/section'
import '../styles/index.scss';
import AuthForm from "../components/form/form";
function Login () {
    const [checked, setCheked] = useState(false);

    function handleCheckbox() {
        setCheked(!checked)
    }

    return (
            <AuthSection>
                <AuthForm heading="Welcome!">
                    <Input id="email" label="Email" type="email" placeholder="name@mail.com"/>
                    <Input id="password" label="Password" type={checked ? "text" : "password"} placeholder="password"/>
                    <Checkbox  checked={checked} handleCheckbox={handleCheckbox} id="show-password" label="Show Password"/>
                    <Button mode="submit">Login</Button>
                </AuthForm>
            </AuthSection>
    )
}

export default Login;
