import { BrowserRouter, Routes, Route } from 'react-router-dom';
import  Login  from './pages/sign-in';
import  Register  from './pages/sign-up';
import ForgotPass from './pages/forgot-password';
import ResetPass from './pages/reset-password';

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
            <Route index element={<Login/>}/>
            <Route path='/sign-in' element={<Login/>}/>
            <Route path='/sign-up'element={<Register/>}/>
            <Route path='/forgot-password'element={<ForgotPass/>}/>
            <Route path='/reset-password'element={<ResetPass/>}/>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
